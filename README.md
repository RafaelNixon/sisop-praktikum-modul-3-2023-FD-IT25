# Sisop Praktikum Modul 3 2023 FD IT25

## Soal 2
**kalian.c**
```
#include <stdio.h>
#include <stdlib.h>
#include <sys/shm.h>
#include <unistd.h>
#include <time.h>

#define ROWS1 4
#define COLS1 2
#define ROWS2 2
#define COLS2 5

int main()
{
    int shmid;
    key_t key = 1234;
    int (*result)[COLS2];

    // Buat shared memory
    shmid = shmget(key, sizeof(int[ROWS1][COLS2]), IPC_CREAT | 0666);
    if (shmid == -1) {
        perror("Failed to create shared memory");
        exit(EXIT_FAILURE);
    }

    // Temeplin shared memory
    result = shmat(shmid, NULL, 0);
    if (result == (int *) -1) {
        perror("Failed to attach shared memory");
        exit(EXIT_FAILURE);
    }

    int matrix1[ROWS1][COLS1];
    int matrix2[ROWS2][COLS2];
    int i, j, k;

    // Bikin matrix1
    srand(time(NULL));
    for (i = 0; i < ROWS1; i++) {
        for (j = 0; j < COLS1; j++) {
            matrix1[i][j] = rand() % 5 + 1;
        }
    }

    // Bikin matrix2
    for (i = 0; i < ROWS2; i++) {
        for (j = 0; j < COLS2; j++) {
            matrix2[i][j] = rand() % 4 + 1;
        }
    }

    // Perkalian matrix terus masukkan ke shared memory
    for (i = 0; i < ROWS1; i++) {
        for (j = 0; j < COLS2; j++) {
            result[i][j] = 0;
            for (k = 0; k < ROWS2; k++) {
                result[i][j] += matrix1[i][k] * matrix2[k][j];
            }
        }
    }

    // Display result matrix
    printf("Hasil:\n");
    for (i = 0; i < ROWS1; i++) {
        for (j = 0; j < COLS2; j++) {
            printf("%d ", result[i][j]);
        }
        printf("\n");
    }

    // Lepas shared memory
    shmdt(result);

    return 0;
}

```
### -Penjelasan-
```
#include <stdio.h>
#include <stdlib.h>
#include <sys/shm.h>
#include <unistd.h>
#include <time.h>
```
- [ ] Melakukan import library yang dibutuhkan

```
#define ROWS1 4
#define COLS1 2
#define ROWS2 2
#define COLS2 5
```
- [ ] Mendefinisikan baris dan kolom untuk matrix yang akan digunakan

```
shmid = shmget(key, sizeof(int[ROWS1][COLS2]), IPC_CREAT | 0666);
    if (shmid == -1) {
        perror("Failed to create shared memory");
        exit(EXIT_FAILURE);
    }
```
- [ ] Membuat shared memory

```
    result = shmat(shmid, NULL, 0);
    if (result == (int *) -1) {
        perror("Failed to attach shared memory");
        exit(EXIT_FAILURE);
    }
```
- [ ] Menempelkan nilai ke shared memory

```
    int matrix1[ROWS1][COLS1];
    int matrix2[ROWS2][COLS2];
    int i, j, k;
```
- [ ] Menginisialisasikan variabel untuk matrix

```
    srand(time(NULL));
    for (i = 0; i < ROWS1; i++) {
        for (j = 0; j < COLS1; j++) {
            matrix1[i][j] = rand() % 5 + 1;
        }
    }
```
- [ ] Membuat matrix pertama dengan nilai acak

```
    for (i = 0; i < ROWS2; i++) {
        for (j = 0; j < COLS2; j++) {
            matrix2[i][j] = rand() % 4 + 1;
        }
    }
```
- [ ] Membuat matrix kedua dengan nilai acak

```
    for (i = 0; i < ROWS1; i++) {
        for (j = 0; j < COLS2; j++) {
            result[i][j] = 0;
            for (k = 0; k < ROWS2; k++) {
                result[i][j] += matrix1[i][k] * matrix2[k][j];
            }
        }
    }
```
- [ ] Mengalikan matrix pertama dan kedua lalu memasukkannya ke shared memory

```
    printf("Hasil:\n");
    for (i = 0; i < ROWS1; i++) {
        for (j = 0; j < COLS2; j++) {
            printf("%d ", result[i][j]);
        }
        printf("\n");
    }
```
- [ ] Menampilkan hasil dari perkalian matrix

```
    shmdt(result);
```
- [ ] Melepas shared memory


**cinta.c**
```
#include <stdio.h>
#include <stdlib.h>
#include <sys/shm.h>
#include <unistd.h>
#include <pthread.h>
#include <time.h>

#define ROWS1 4
#define COLS1 2
#define ROWS2 2
#define COLS2 5

int faktorial(int n) {
    int result = 1;
    for(int i = 1; i <= n; i++) {
        result *= i;
    }
    return result;
}

struct arg_struct {
    int row;
    int col;
    int *result;
};

void *faktorial_thread(void *arguments) {
    struct arg_struct *args = arguments;
    int row = args->row;
    int col = args->col;
    int *result = args->result;
    result[row*COLS2 + col] = faktorial(result[row*COLS2 + col]);
    pthread_exit(NULL);
}

int main() {
    
    //Timer
    struct timespec start, end;
    long long waktu;
    clock_gettime(CLOCK_MONOTONIC_RAW, &start);
    //ETimer1

    int shmid;
    key_t key = 1234;
    int (*result)[COLS2];

    // Tempelin shared memory
    shmid = shmget(key, sizeof(int[ROWS1][COLS2]), 0666);
    if (shmid == -1) {
        perror("Failed to open shared memory");
        exit(EXIT_FAILURE);
    }
    result = shmat(shmid, NULL, 0);
    if (result == (int *) -1) {
        perror("Failed to attach shared memory");
        exit(EXIT_FAILURE);
    }

    // Tampilkan matrix
    printf("Hasil Matrix:\n");
    for (int i = 0; i < ROWS1; i++) {
        for (int j = 0; j < COLS2; j++) {
            printf("%d ", result[i][j]);
        }
        printf("\n");
    }

    // Hitung faktorial
    pthread_t threads[ROWS1*COLS2];
    struct arg_struct args[ROWS1*COLS2];
    int thread_count = 0;

    for (int i = 0; i < ROWS1; i++) {
        for (int j = 0; j < COLS2; j++) {
            args[thread_count].row = i;
            args[thread_count].col = j;
            args[thread_count].result = (int *) result;
            pthread_create(&threads[thread_count], NULL, faktorial_thread, (void *)&args[thread_count]);
            thread_count++;
        }
    }

    // Tunggu thread
    for (int i = 0; i < thread_count; i++) {
        pthread_join(threads[i], NULL);
    }

    // Display faktorial matrix
    printf("Faktorial:\n");
    for (int i = 0; i < ROWS1; i++) {
        for (int j = 0; j < COLS2; j++) {
            printf("%d ", result[i][j]);
        }
        printf("\n");
    }

    // Lepas shared memory
    shmdt(result);

    //Timer
    clock_gettime(CLOCK_MONOTONIC_RAW, &end);
    waktu = (end.tv_sec - start.tv_sec) * 1000000000 + (end.tv_nsec - start.tv_nsec);
    printf("Elapsed time: %lld nanoseconds atau %ld miliseconds atau %d seconds\n", waktu, waktu/1000000, waktu/1000000000);
    //ETimer2

    return 0;
}

```
### -Penjelasan-
```
#include <stdio.h>
#include <stdlib.h>
#include <sys/shm.h>
#include <unistd.h>
#include <pthread.h>
#include <time.h>
```
- [ ] Melakukan import library yang dibutuhkan

```
#define ROWS1 4
#define COLS1 2
#define ROWS2 2
#define COLS2 5
```
- [ ] Mendefinisikan baris dan kolom untuk matrix yang akan digunakan

```
int faktorial(int n) {
    int result = 1;
    for(int i = 1; i <= n; i++) {
        result *= i;
    }
    return result;
}
```
- [ ] Mendefinisikan fungsi faktorial yang akan mengembalikan hasil berupa integer dari faktorial variabel n

```
struct arg_struct {
    int row;
    int col;
    int *result;
};
```
- [ ] Mendefinisikan struct baru bernama arg_struct untuk digunakan dalam thread

```
void *faktorial_thread(void *arguments) {
    struct arg_struct *args = arguments;
    int row = args->row;
    int col = args->col;
    int *result = args->result;
    result[row*COLS2 + col] = faktorial(result[row*COLS2 + col]);
    pthread_exit(NULL);
}
```
- [ ] Mendefinisikan fungsi faktorial_thread untuk melakukan faktorial menggunakan thread

```
    struct timespec start, end;
    long long waktu;
    clock_gettime(CLOCK_MONOTONIC_RAW, &start);
```
- [ ] Mendefinisikan variabel yang akan digunakan untuk timer/pewaktu

```
    shmid = shmget(key, sizeof(int[ROWS1][COLS2]), 0666);
    if (shmid == -1) {
        perror("Failed to open shared memory");
        exit(EXIT_FAILURE);
    }
    result = shmat(shmid, NULL, 0);
    if (result == (int *) -1) {
        perror("Failed to attach shared memory");
        exit(EXIT_FAILURE);
    }
```
- [ ] Melihat tempelan nilai di shared memory dari kalian.c

```
    printf("Hasil Matrix:\n");
    for (int i = 0; i < ROWS1; i++) {
        for (int j = 0; j < COLS2; j++) {
            printf("%d ", result[i][j]);
        }
        printf("\n");
    }
```
- [ ] Menampilkan matrix ke console

```
    pthread_t threads[ROWS1*COLS2];
    struct arg_struct args[ROWS1*COLS2];
    int thread_count = 0;

    for (int i = 0; i < ROWS1; i++) {
        for (int j = 0; j < COLS2; j++) {
            args[thread_count].row = i;
            args[thread_count].col = j;
            args[thread_count].result = (int *) result;
            pthread_create(&threads[thread_count], NULL, faktorial_thread, (void *)&args[thread_count]);
            thread_count++;
        }
    }
```
- [ ] Menghitung faktorial dengan thread

```
    for (int i = 0; i < thread_count; i++) {
        pthread_join(threads[i], NULL);
    }
```
- [ ] Menunggu thread selesai

```
    printf("Faktorial:\n");
    for (int i = 0; i < ROWS1; i++) {
        for (int j = 0; j < COLS2; j++) {
            printf("%d ", result[i][j]);
        }
        printf("\n");
    }
```
- [ ] Menampilkan hasil faktorial ke console

```
    shmdt(result);
```
- [ ] Melepas shared memory

```
    clock_gettime(CLOCK_MONOTONIC_RAW, &end);
    waktu = (end.tv_sec - start.tv_sec) * 1000000000 + (end.tv_nsec - start.tv_nsec);
    printf("Elapsed time: %lld nanoseconds atau %ld miliseconds atau %d seconds\n", waktu, waktu/1000000, waktu/1000000000);
```
- [ ] Menghitung total waktu sejak start hingga end dan menampilkannya dalam nanosecond, milisecond, dan second melalui console


**sisop.c**
```
#include <stdio.h>
#include <stdlib.h>
#include <sys/shm.h>
#include <time.h>

#define ROWS1 4
#define COLS1 2
#define ROWS2 2
#define COLS2 5

int faktorial(int n) {
    int result = 1;
    for(int i = 1; i <= n; i++) {
        result *= i;
    }
    return result;
}

int main() {

    //Timer
    struct timespec start, end;
    long long waktu;
    clock_gettime(CLOCK_MONOTONIC_RAW, &start);
    //ETimer1

    int shmid;
    key_t key = 1234;
    int (*result)[COLS2];

    // Tempelin shared memory
    shmid = shmget(key, sizeof(int[ROWS1][COLS2]), 0666);
    if (shmid == -1) {
        perror("Failed to open shared memory");
        exit(EXIT_FAILURE);
    }
    result = shmat(shmid, NULL, 0);
    if (result == (int *) -1) {
        perror("Failed to attach shared memory");
        exit(EXIT_FAILURE);
    }

    // Tampilkan matrix
    printf("Hasil Matrix:\n");
    for (int i = 0; i < ROWS1; i++) {
        for (int j = 0; j < COLS2; j++) {
            printf("%d ", result[i][j]);
        }
        printf("\n");
    }

    // Hitung faktorial
    for (int i = 0; i < ROWS1; i++) {
        for (int j = 0; j < COLS2; j++) {
            result[i][j] = faktorial(result[i][j]);
        }
    }

    // Tampilkan faktorial matix
    printf("Faktorial:\n");
    for (int i = 0; i < ROWS1; i++) {
        for (int j = 0; j < COLS2; j++) {
            printf("%d ", result[i][j]);
        }
        printf("\n");
    }

    // Lepas shared memory
    shmdt(result);

    //Timer
    clock_gettime(CLOCK_MONOTONIC_RAW, &end);
    waktu = (end.tv_sec - start.tv_sec) * 1000000000 + (end.tv_nsec - start.tv_nsec);
    printf("Elapsed time: %lld nanoseconds atau %ld miliseconds atau %d seconds\n", waktu, waktu/1000000, waktu/1000000000);
    //ETimer2

    return 0;
}

```
### -Penjelasan-

```
#include <stdio.h>
#include <stdlib.h>
#include <sys/shm.h>
#include <time.h>
```
- [ ] Melakukan import library yang dibutuhkan

```
#define ROWS1 4
#define COLS1 2
#define ROWS2 2
#define COLS2 5
```
- [ ] Mendefinisikan baris dan kolom untuk matrix yang akan digunakan

```
int faktorial(int n) {
    int result = 1;
    for(int i = 1; i <= n; i++) {
        result *= i;
    }
    return result;
}
```
- [ ] Mendefinisikan fungsi faktorial yang akan mengembalikan hasil berupa integer dari faktorial variabel n

```
    struct timespec start, end;
    long long waktu;
    clock_gettime(CLOCK_MONOTONIC_RAW, &start);
```
- [ ] Mendefinisikan variabel yang akan digunakan untuk timer/pewaktu

```
    shmid = shmget(key, sizeof(int[ROWS1][COLS2]), 0666);
    if (shmid == -1) {
        perror("Failed to open shared memory");
        exit(EXIT_FAILURE);
    }
    result = shmat(shmid, NULL, 0);
    if (result == (int *) -1) {
        perror("Failed to attach shared memory");
        exit(EXIT_FAILURE);
    }
```
- [ ] Melihat tempelan nilai di shared memory dari kalian.c

```
    printf("Hasil Matrix:\n");
    for (int i = 0; i < ROWS1; i++) {
        for (int j = 0; j < COLS2; j++) {
            printf("%d ", result[i][j]);
        }
        printf("\n");
    }
```
- [ ] Menampilkan matrix ke console

```
    for (int i = 0; i < ROWS1; i++) {
        for (int j = 0; j < COLS2; j++) {
            result[i][j] = faktorial(result[i][j]);
        }
    }
```
- [ ] Menghitung faktorial secara biasa (tanpa thread)

```
    printf("Faktorial:\n");
    for (int i = 0; i < ROWS1; i++) {
        for (int j = 0; j < COLS2; j++) {
            printf("%d ", result[i][j]);
        }
        printf("\n");
    }
```
- [ ] Menampilkan hasil faktorial ke console

```
    shmdt(result);
```
- [ ] Melepas shared memory

```
    clock_gettime(CLOCK_MONOTONIC_RAW, &end);
    waktu = (end.tv_sec - start.tv_sec) * 1000000000 + (end.tv_nsec - start.tv_nsec);
    printf("Elapsed time: %lld nanoseconds atau %ld miliseconds atau %d seconds\n", waktu, waktu/1000000, waktu/1000000000);
```
- [ ] Menghitung total waktu sejak start hingga end dan menampilkannya dalam nanosecond, milisecond, dan second melalui console

