#include <stdio.h>
#include <stdlib.h>
#include <sys/shm.h>
#include <time.h>

#define ROWS1 4
#define COLS1 2
#define ROWS2 2
#define COLS2 5

int faktorial(int n) {
    int result = 1;
    for(int i = 1; i <= n; i++) {
        result *= i;
    }
    return result;
}

int main() {

    //Timer
    struct timespec start, end;
    long long waktu;
    clock_gettime(CLOCK_MONOTONIC_RAW, &start);
    //ETimer1

    int shmid;
    key_t key = 1234;
    int (*result)[COLS2];

    // Tempelin shared memory
    shmid = shmget(key, sizeof(int[ROWS1][COLS2]), 0666);
    if (shmid == -1) {
        perror("Failed to open shared memory");
        exit(EXIT_FAILURE);
    }
    result = shmat(shmid, NULL, 0);
    if (result == (int *) -1) {
        perror("Failed to attach shared memory");
        exit(EXIT_FAILURE);
    }

    // Tampilkan matrix
    printf("Hasil Matrix:\n");
    for (int i = 0; i < ROWS1; i++) {
        for (int j = 0; j < COLS2; j++) {
            printf("%d ", result[i][j]);
        }
        printf("\n");
    }

    // Hitung faktorial
    for (int i = 0; i < ROWS1; i++) {
        for (int j = 0; j < COLS2; j++) {
            result[i][j] = faktorial(result[i][j]);
        }
    }

    // Tampilkan faktorial matix
    printf("Faktorial:\n");
    for (int i = 0; i < ROWS1; i++) {
        for (int j = 0; j < COLS2; j++) {
            printf("%d ", result[i][j]);
        }
        printf("\n");
    }

    // Lepas shared memory
    shmdt(result);

    //Timer
    clock_gettime(CLOCK_MONOTONIC_RAW, &end);
    waktu = (end.tv_sec - start.tv_sec) * 1000000000 + (end.tv_nsec - start.tv_nsec);
    printf("Elapsed time: %lld nanoseconds atau %ld miliseconds atau %d seconds\n", waktu, waktu/1000000, waktu/1000000000);
    //ETimer2

    return 0;
}
