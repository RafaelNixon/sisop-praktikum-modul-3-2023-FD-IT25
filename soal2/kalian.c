#include <stdio.h>
#include <stdlib.h>
#include <sys/shm.h>
#include <unistd.h>
#include <time.h>

#define ROWS1 4
#define COLS1 2
#define ROWS2 2
#define COLS2 5

int main()
{
    int shmid;
    key_t key = 1234;
    int (*result)[COLS2];

    // Buat shared memory
    shmid = shmget(key, sizeof(int[ROWS1][COLS2]), IPC_CREAT | 0666);
    if (shmid == -1) {
        perror("Failed to create shared memory");
        exit(EXIT_FAILURE);
    }

    // Temeplin shared memory
    result = shmat(shmid, NULL, 0);
    if (result == (int *) -1) {
        perror("Failed to attach shared memory");
        exit(EXIT_FAILURE);
    }

    int matrix1[ROWS1][COLS1];
    int matrix2[ROWS2][COLS2];
    int i, j, k;

    // Bikin matrix1
    srand(time(NULL));
    for (i = 0; i < ROWS1; i++) {
        for (j = 0; j < COLS1; j++) {
            matrix1[i][j] = rand() % 5 + 1;
        }
    }

    // Bikin matrix2
    for (i = 0; i < ROWS2; i++) {
        for (j = 0; j < COLS2; j++) {
            matrix2[i][j] = rand() % 4 + 1;
        }
    }

    // Perkalian matrix terus masukkan ke shared memory
    for (i = 0; i < ROWS1; i++) {
        for (j = 0; j < COLS2; j++) {
            result[i][j] = 0;
            for (k = 0; k < ROWS2; k++) {
                result[i][j] += matrix1[i][k] * matrix2[k][j];
            }
        }
    }

    // Display result matrix
    printf("Hasil:\n");
    for (i = 0; i < ROWS1; i++) {
        for (j = 0; j < COLS2; j++) {
            printf("%d ", result[i][j]);
        }
        printf("\n");
    }

    // Lepas shared memory
    shmdt(result);

    return 0;
}
